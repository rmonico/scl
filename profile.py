#!/usr/bin/env python3
# coding=UTF-8


from os import getenv


class Profile:
    pass


class ProfileFactory(object):

    def __init__(self, profile_folder):
        environment_variable = getenv('SCL_PROFILE_FOLDER')
        # TODO Acho que não vai funcionar no Windows...
        fallback_folder = getenv('HOME') + '/.sclprofiles'

        self.profile_folder = profile_folder or environment_variable or fallback_folder

    def make(self, profile_name):
        raw_profile = load_properties(
            '{}/{}.profile'.format(self.profile_folder, profile_name))

        profile = Profile()

        profile.description = raw_profile["description"]
        profile.user = raw_profile["user"]
        profile.password = raw_profile["password"]
        profile.host = raw_profile["host"]
        profile.port = raw_profile["port"]
        profile.serviceid = raw_profile["serviceid"]
        profile.pager = raw_profile["pager"]

        return profile


def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"')
                props[key] = value

    return props
