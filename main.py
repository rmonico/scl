#!/usr/bin/env python3
# coding=UTF-8


import cx_Oracle
import argparse
from profile import ProfileFactory
from pathlib import Path


def log(minimum_level, message, *args_):
    global args

    if args.verbosity >= minimum_level:
        print(message.format(*args_))


def make_args():
    parser = argparse.ArgumentParser(
        prog='scl', description='Run a script on an Oracle database')

    # TODO Limit here row count
    # TODO Paginate results
    # TODO Implement pager
    # TODO Função para retornar todas as tabelas
    # TODO Função para descrever a estrutura de uma tabela
    # TODO Configurações globais: pager, NLS_LANG, profile, column_separator, null_value
    # TODO Handle void-return statements (insert, update, etc)
    parser.add_argument('profile', type=str,
                        help='Profile to use to connect to database')

    parser.add_argument('statement', type=str,
                        help='Statement to run on database')

    parser.add_argument('--profile-folder', '-pf', type=str,
                        help='Folder to look for profiles')

    parser.add_argument('--column-separator', '-cs', dest='separator',
                        type=str, default=' | ', help='Column separator')

    parser.add_argument('--null-value', '-nv', type=str, default='*null*',
                        help='Value to output when find a null')

    parser.add_argument('--dont-align-columns', '-da', action='store_true',
                        default=False, help='Align columns')

    parser.add_argument('--no-header', '-nh', action='store_true',
                        default=False, help='Dont show headers')

    verbosity = parser.add_argument('--verbose', '-v', dest='verbosity',
                                    default=0, action='count',
                                    help='Enable verbosity (up to three levels)')

    global args

    args = parser.parse_args()

    if args.verbosity > 3:
        raise argparse.ArgumentError(verbosity,
                                     "Should be not greather than 3")

    log(2, 'Verbose mode enabled (level {})', args.verbosity)

    return args


def make_profile():
    global args

    factory = ProfileFactory(args.profile_folder)

    log(1, 'Profile folder: {}', factory.profile_folder)

    return factory.make(args.profile)


def get_statement(raw_statement):
    statement_file = Path(raw_statement)

    if statement_file.is_file():
        file = open(statement_file, 'rt')

        return file.read()
    else:
        return raw_statement


def make_connection(profile):
    connection_string = '{}/{}@{}:{}/{}'.format(
        profile.user, profile.password, profile.host, profile.port, profile.
        serviceid)

    if args.verbosity >= 1:
        print('connection_string: {}'.format(connection_string))

    return cx_Oracle.connect(connection_string)


def cache_results(cursor):
    results = []

    if not args.no_header:
        header = []

        for column_name in cursor.description:
            header.append(column_name[0])

        results.append(header)

    result = cursor.fetchone()

    if result != None:
        while result:
            line = []

            for data in result:
                line.append(data)

            results.append(line)

            result = cursor.fetchone()

    return results


def format_results(results):
    formatted_results = []

    for row in results:
        formatted_row = []
        for data in row:
            if data:
                formatted_row.append(str(data))
            else:
                # FIXME Está tratando zero como nulo na query abaixo
                # select val_pgto, vl_saldo from movpgto x where tip_mpgto = '2' and vl_saldo is null
                formatted_row.append(args.null_value)

        formatted_results.append(formatted_row)

    return formatted_results


def calculate_column_widths(formatted_results):
    if len(formatted_results) == 0:
        return []

    column_widths = [-1 for i in range(len(formatted_results[0]))]

    for row in formatted_results:
        for i, data in enumerate(row):
            if len(data) > column_widths[i]:
                column_widths[i] = len(data)

    return column_widths


def fill_with_spaces(text, final_size):
    return text + (" " * (final_size - len(text)))


def print_results(results, column_widths):
    for row in results:
        line = ''
        for i, data in enumerate(row):
            #TODO Handle '\n' character on str
            if column_widths:
                line += fill_with_spaces(str(data), column_widths[i])
            else:
                line += str(data)

            if i < len(row):
                line += args.separator

        print(line)


def run_query(connection):
    cursor = connection.cursor()

    statement = get_statement(args.statement)

    log(1, "Running '{}'", statement)

    cursor.execute(statement)

    results = cache_results(cursor)

    formatted_results = format_results(results)

    if not args.dont_align_columns:
        column_widths = calculate_column_widths(formatted_results)
    else:
        column_widths = None

    print_results(formatted_results, column_widths)

    cursor.close()
    connection.close()


def main():
    global args

    args = make_args()

    profile = make_profile()

    log(2, 'description={}', profile.description)
    log(3, 'user={}', profile.user)
    log(3, 'password={}', profile.password)
    log(3, 'host={}', profile.host)
    log(3, 'port={}', profile.port)
    log(3, 'serviceid={}', profile.serviceid)
    log(3, 'pager={}', profile.pager)

    connection = make_connection(profile)

    run_query(connection)


if __name__ == '__main__':
    main()
